 #include <iostream>
 #include "Item.h"

 using std::cout;
 using std::endl;
 
 std::string getItemString(const Item& item)
 {
 	return
 		"[Serial: " + item.getSerial() +
        ", Name: " + item.getName() +
 		", Category: " + getItemCategoryString(item.getCategory()) +
 		", Price: " + std::to_string(item.getPrice()) +
 		", Amount: " + std::to_string(item.getCount()) + "]";
 }

 int test1Item()
 {

 	try
 	{
 		// Tests Ex10 part 1 - Item

 		cout <<
 			"*******************\n" <<
 			"Test 1 - Item				\n" <<
 			"*******************\n" << endl;


 		cout <<
 			"Initializing Item1: ... \n" << endl;

 		Item item1("White Sliced Bread 750g", "BFTRZ", 5.12, FOOD);
 		std::string expected = "[Serial: BFTRZ, Name: White Sliced Bread 750g, Category: Food, Price: 5.120000, Amount: 1]";
 		std::string got = getItemString(item1);
        if (got == expected)
        {
            cout << "\033[1;32mTest Passed\033[0m\n \n" << endl;
        }
        else
        {
            cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
            cout << "Expected: " << expected << endl;
            cout << "Got     : " << got << endl;
            std::cout << " \n" << endl;
            system("./printMessage 10101");
            std::cout << " \n" << endl;
            return 10101;
        }

        cout <<
            "\nInitializing Item2: ... \n" << endl;

        Item item2("Table Salt 1kg", "vT0LC", 2.07, FOOD);
        expected = "[Serial: vT0LC, Name: Table Salt 1kg, Category: Food, Price: 2.070000, Amount: 1]";
        got = getItemString(item2);
        if (got == expected)
        {
            cout << "\033[1;32mTest Passed\033[0m\n \n" << endl;
        }
        else
        {
            cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
            cout << "Expected: " << expected << endl;
            cout << "Got     : " << got << endl;
            std::cout << " \n" << endl;
            system("./printMessage 10101");
            std::cout << " \n" << endl;
            return 10101;
        }

        cout <<
            "\nInitializing Item3: ... \n" << endl;

        Item item3("Milk 3% 1 liter", "USlOG", 5.17, FOOD);
        expected = "[Serial: USlOG, Name: Milk 3% 1 liter, Category: Food, Price: 5.170000, Amount: 1]";
        got = getItemString(item3);
        if (got == expected)
        {
            cout << "\033[1;32mTest Passed\033[0m\n \n" << endl;
        }
        else
        {
            cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
            cout << "Expected: " << expected << endl;
            cout << "Got     : " << got << endl;
            std::cout << " \n" << endl;
            system("./printMessage 10101");
            std::cout << " \n" << endl;
            return 10101;
        }

        cout <<
            "\nInitializing Item4: ... \n" << endl;

        Item item4("Office Desk IKEA", "pj77k", 1050.0, HOME);
        expected = "[Serial: pj77k, Name: Office Desk IKEA, Category: Home, Price: 1050.000000, Amount: 1]";
        got = getItemString(item4);
        if (got == expected)
        {
            cout << "\033[1;32mTest Passed\033[0m\n \n" << endl;
        }
        else
        {
            cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
            cout << "Expected: " << expected << endl;
            cout << "Got     : " << got << endl;
            std::cout << " \n" << endl;
            system("./printMessage 10101");
            std::cout << " \n" << endl;
            return 10101;
        }

        cout <<
            "\nInitializing Item5: ... \n" << endl;

        Item item5("Leather Sofa Brown 3 Seats IKEA", "LBbGn", 1099.0, HOME);
        expected = "[Serial: LBbGn, Name: Leather Sofa Brown 3 Seats IKEA, Category: Home, Price: 1099.000000, Amount: 1]";
        got = getItemString(item5);
        if (got == expected)
        {
            cout << "\033[1;32mTest Passed\033[0m\n \n" << endl;
        }
        else
        {
            cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
            cout << "Expected: " << expected << endl;
            cout << "Got     : " << got << endl;
            std::cout << " \n" << endl;
            system("./printMessage 10101");
            std::cout << " \n" << endl;
            return 10101;
        }

        cout <<
            "\nInitializing Item6: ... \n" << endl;

        Item item6("Mouth Wash 500ml", "8sZ4N", 36.9, PHARM);
        expected = "[Serial: 8sZ4N, Name: Mouth Wash 500ml, Category: Pharm, Price: 36.900000, Amount: 1]";
        got = getItemString(item6);
        if (got == expected)
        {
            cout << "\033[1;32mTest Passed\033[0m\n \n" << endl;
        }
        else
        {
            cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
            cout << "Expected: " << expected << endl;
            cout << "Got     : " << got << endl;
            std::cout << " \n" << endl;
            system("./printMessage 10101");
            std::cout << " \n" << endl;
            return 10101;
        }

        cout <<
            "\nInitializing Item7: ... \n" << endl;

        Item item7("Shampoo 750ml", "G9vKR", 10.9, PHARM);
        expected = "[Serial: G9vKR, Name: Shampoo 750ml, Category: Pharm, Price: 10.900000, Amount: 1]";
        got = getItemString(item7);
        if (got == expected)
        {
            cout << "\033[1;32mTest Passed\033[0m\n \n" << endl;
        }
        else
        {
            cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
            cout << "Expected: " << expected << endl;
            cout << "Got     : " << got << endl;
            std::cout << " \n" << endl;
            system("./printMessage 10101");
            std::cout << " \n" << endl;
            return 10101;
        }

        cout <<
            "\nInitializing Item8: ... \n" << endl;

        Item item8("Bleach 2 liter", "boPnl", 12.5, CLEANING);
        expected = "[Serial: boPnl, Name: Bleach 2 liter, Category: Cleaning, Price: 12.500000, Amount: 1]";
        got = getItemString(item8);
        if (got == expected)
        {
            cout << "\033[1;32mTest Passed\033[0m\n \n" << endl;
        }
        else
        {
            cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
            cout << "Expected: " << expected << endl;
            cout << "Got     : " << got << endl;
            std::cout << " \n" << endl;
            system("./printMessage 10101");
            std::cout << " \n" << endl;
            return 10101;
        }

        //////////////////////////////
        // Checking Price and Count //
        //////////////////////////////

 		cout <<
 			"\nSetting Item1 Counter to 5: ... \n" << endl;
 		item1.setCount(5);

        expected = "[Serial: BFTRZ, Name: White Sliced Bread 750g, Category: Food, Price: 5.120000, Amount: 5]";
        got = getItemString(item1);
        if (got == expected)
        {
            cout << "\033[1;32mTest Passed\033[0m\n \n" << endl;
        }
        else
        {
            cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
            cout << "Expected: " << expected << endl;
            cout << "Got     : " << got << endl;
            std::cout << " \n" << endl;
            system("./printMessage 10102");
            std::cout << " \n" << endl;
            return 10102;
        }

        cout <<
            "\nCalculating item1 total price: ... \n" << endl;
        item1.setCount(5);

        expected = "25.600000";
        got = std::to_string(item1.totalPrice());
        if (got == expected)
        {
            cout << "\033[1;32mTest Passed\033[0m\n \n" << endl;
        }
        else
        {
            cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
            cout << "Expected: " << expected << endl;
            cout << "Got     : " << got << endl;
            std::cout << " \n" << endl;
            system("./printMessage 10103");
            std::cout << " \n" << endl;
            return 10103;
        }

        cout <<
            "\nSetting Item7 Counter to 34 and Calculating item7 total price: ... \n" << endl;
        item7.setCount(34);

        expected = "370.600000";
        got = std::to_string(item7.totalPrice());
        if (got == expected)
        {
            cout << "\033[1;32mTest Passed\033[0m\n \n" << endl;
        }
        else
        {
            cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
            cout << "Expected: " << expected << endl;
            cout << "Got     : " << got << endl;
            std::cout << " \n" << endl;
            system("./printMessage 10104");
            std::cout << " \n" << endl;
            return 10104;
        }

        ////////////////////////////
        // Checking Bad Arguments //
        ////////////////////////////

        try
		{
            cout <<
                "\nTrying to initialize a bad item: ..." << endl;
            cout << "[Serial: y3hMm, Name: Eye Contacts liquid 350ml, Category: Pharm, Price: -49.900000, Amount: 1]\n";

            Item badItem1("Eye Contacts liquid 350ml", "y3hMm", -49.9, PHARM);


            cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
            std::cout << " \n" << endl;
            system("./printMessage 10105");
            std::cout << " \n" << endl;
            return 10105;
		}
		catch (const std::invalid_argument& e)
		{
            cout << "\033[1;32mOK - Got std::invalid_argument\033[0m\n \n" << endl;
		}
		catch (const std::exception& e)
		{
            cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
            std::cout << " \n" << endl;
            cout << "\033[1;31m" << "Got <" << typeid(e).name() << ">" "\033[0m\n \n" << endl;
            std::cout << " \n" << endl;
            system("./printMessage 10106");
            std::cout << " \n" << endl;
            return 10106;
		}
		catch (...)
		{
            cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
            std::cout << " \n" << endl;
            system("./printMessage 10107");
            std::cout << " \n" << endl;
            return 10107;
		}

        try
        {
            cout <<
                "\nTrying to initialize a bad item: ..." << endl;
            cout << "[Serial: abcdefg, Name: Dishwash Soap 515ml, Category: Cleaning, Price: 13.9, Amount: 1]\n";

            Item badItem2("Dishwash Soap 515ml", "abcdefg", 13.9, CLEANING);

            cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
            std::cout << " \n" << endl;
            system("./printMessage 10108");
            std::cout << " \n" << endl;
            return 10108;
        }
        catch (const std::invalid_argument& e)
        {
            cout << "\033[1;32mOK - Got std::invalid_argument\033[0m\n \n" << endl;
        }
        catch (const std::exception& e)
        {
            cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
            std::cout << " \n" << endl;
            cout << "\033[1;31m" << "Got <" << typeid(e).name() << ">" "\033[0m\n \n" << endl;
            std::cout << " \n" << endl;
            system("./printMessage 10109");
            std::cout << " \n" << endl;
            return 10109;
        }
        catch (...)
        {
            cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
            std::cout << " \n" << endl;
            system("./printMessage 10110");
            std::cout << " \n" << endl;
            return 10110;
        }

        try
        {
            cout <<
                "\nTrying to set a bad parameter to item4: ..." << endl;
            cout << "item4.setCount(0)\n";
            cout <<
                "\nItem's count must be positive - Expecting std::invalid_argument... \n" << endl;

            item4.setCount(-1);

            cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
            std::cout << " \n" << endl;
            system("./printMessage 10111");
            std::cout << " \n" << endl;
            return 10111;
        }
        catch (const std::invalid_argument& e)
        {
            cout << "\033[1;32mOK - Got std::invalid_argument\033[0m\n \n" << endl;
        }
        catch (const std::exception& e)
        {
            cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
            std::cout << " \n" << endl;
            cout << "\033[1;31m" << "Got <" << typeid(e).name() << ">" "\033[0m\n \n" << endl;
            std::cout << " \n" << endl;
            system("./printMessage 10112");
            std::cout << " \n" << endl;
            return 10112;
        }
        catch (...)
        {
            cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
            std::cout << " \n" << endl;
            system("./printMessage 10113");
            std::cout << " \n" << endl;
            return 10113;
        }

        try
        {
            cout <<
                "\nTrying to set a bad parameter to item6: ..." << endl;
            cout << "item6.setCount(-10)\n";
            cout <<
                "\nItem's count must be positive - Expecting std::invalid_argument... \n" << endl;

            item6.setCount(-10);

            cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
            std::cout << " \n" << endl;
            system("./printMessage 10111");
            std::cout << " \n" << endl;
            return 10111;
        }
        catch (const std::invalid_argument& e)
        {
            cout << "\033[1;32mOK - Got std::invalid_argument\033[0m\n \n" << endl;
        }
        catch (const std::exception& e)
        {
            cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
            std::cout << " \n" << endl;
            cout << "\033[1;31m" << "Got <" << typeid(e).name() << ">" "\033[0m\n \n" << endl;
            std::cout << " \n" << endl;
            system("./printMessage 10112");
            std::cout << " \n" << endl;
            return 10112;
        }
        catch (...)
        {
            cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
            std::cout << " \n" << endl;
            system("./printMessage 10113");
            std::cout << " \n" << endl;
            return 10113;
        }

        ///////////////////////////////
        // Checking Operators <,>,== //
        ///////////////////////////////

        cout <<
            "\nComparing items: ... \n \n" << endl;

        cout <<
            "item1 < item2 --> ";

        bool result = item1 < item2;

        if (result == true)
        {
            cout << "\033[1;32mOK\033[0m\n \n" << endl;
        }
        else
        {
            cout << "\033[1;31mNOT OK\033[0m\n \n" << endl;
            std::cout << " \n" << endl;
            system("./printMessage 10114");
            std::cout << " \n" << endl;
            return 10114;
        }

        cout <<
            "item5 > item7 --> ";

        result = item5 > item7;

        if (result == true)
        {
            cout << "\033[1;32mOK\033[0m\n \n" << endl;
        }
        else
        {
            cout << "\033[1;31mNOT OK\033[0m\n \n" << endl;
            std::cout << " \n" << endl;
            system("./printMessage 10115");
            std::cout << " \n" << endl;
            return 10115;
        }

        cout <<
            "item1 == item2 --> ";

        result = item1 == item2;

        if (result == false)
        {
            cout << "\033[1;32mOK\033[0m\n \n" << endl;
        }
        else
        {
            cout << "\033[1;31mNOT OK\033[0m\n \n" << endl;
            std::cout << " \n" << endl;
            system("./printMessage 10116");
            std::cout << " \n" << endl;
            return 10116;
        }

        cout <<
            "item8 < item7 --> ";

        result = item8 < item7;

        if (result == false)
        {
            cout << "\033[1;32mOK\033[0m\n \n" << endl;
        }
        else
        {
            cout << "\033[1;31mNOT OK\033[0m\n \n" << endl;
            std::cout << " \n" << endl;
            system("./printMessage 10117");
            std::cout << " \n" << endl;
            return 10117;
        }

        cout <<
            "item5 > item2 --> ";

        result = item5 > item2;

        if (result == false)
        {
            cout << "\033[1;32mOK\033[0m\n \n" << endl;
        }
        else
        {
            cout << "\033[1;31mNOT OK\033[0m\n \n" << endl;
            std::cout << " \n" << endl;
            system("./printMessage 10118");
            std::cout << " \n" << endl;
            return 10118;
        }

        result = item3 > item8;
        cout <<
            "item3 > item8 --> ";
        if (result == false)
        {
            cout << "\033[1;32mOK\033[0m\n \n" << endl;
        }
        else
        {
            cout << "\033[1;31mNOT OK\033[0m\n \n" << endl;
            std::cout << " \n" << endl;
            system("./printMessage 10119");
            std::cout << " \n" << endl;
            return 10119;
        }

 	}
    catch (...)
    {
        std::cerr << "\033[1;31mTest crashed\033[0m\n \n" << endl;
        std::cerr << "\033[1;31mFAILED: The program crashed, check the following things:\n\033[0m\n \n" <<
            "1. Did you delete a pointer twice?\n2. Did you access index out of bounds?\n" <<
            "3. Did you remember to initialize array before accessing it?";
        return 2;
    }

    return 0;

 }

 int main()
 {
 	std::cout <<
 		"###########################\n" <<
 		"Exercise 10 - Magshistore\n" <<
 		"Part 1 - Item\n" << 
 		"###########################\n" << std::endl;

 	int testResult = test1Item();

    cout << (testResult == 0 ? "\033[1;32m \n****** Ex10 Part 1 Tests Passed ******\033[0m\n \n" : "\033[1;31mEx10 Part 1 Tests Failed\033[0m\n \n") << endl;

    return testResult;
 }