#include <iostream>
#include <fstream>
#include <sstream>

#include "Store.h"
#include "Customer.h"

using std::cout;
using std::endl;

std::string getItemString(const Item& item)
{
    return
        "[Serial: " + item.getSerial() +
        ", Name: " + item.getName() +
        ", Category: " + getItemCategoryString(item.getCategory()) +
        ", Price: " + std::to_string(item.getPrice()) +
        ", Amount: " + std::to_string(item.getCount()) + "]";
}

std::string getShoppingCartString(std::set<Item>* shoppingCart, const std::string shoppingCartName)
{
    std::string result = "Items in shopping cart: " + shoppingCartName + "\n";
    for (auto it = shoppingCart->begin(); it != shoppingCart->end(); it++)
    {
        result += getItemString(*it) + "\n";
    }
    return result;
}

std::string readFileToString(const std::string fileName)
{
    std::ifstream inFile;
    inFile.open(fileName); //open the input file

    std::stringstream strStream;
    strStream << inFile.rdbuf(); //read the file
    std::string str = strStream.str(); //str holds the content of the file

    return str;
}



int test4Memory()
{

    try
    {
        // Memory Test for Ex10 

        cout <<
            "*******************\n" <<
            "Test 4 - Memory	\n" <<
            "*******************\n" << endl;

        cout <<
            "Initializing 2 stores\n" <<
            "Store1(\"MagshIKEA\", \"InventoryIKEA.csv\"): ... \n" <<
            "Store2(\"Shefa Isaschar\", \"InventorySuperMarket.csv\"): ... \n" << endl;

        Store s1("MagshIKEA", "InventoryIKEA.csv");
        Store s2("Shefa Isaschar", "InventorySuperMarket.csv");

        cout <<
            "\nInitializing customer c1(\"Moti Vazia\"): ... \n" << endl;

        Customer c1("Moti Vazia");

        cout << "\033[1;32mOK\033[0m\n \n" << endl;

        const std::string shoppingCart1Name = "HomeStuff";

        cout <<
            "\nCreating a new shopping cart for the customer - c1.createNewShoppingCart(\"HomeStuff\"): ... \n" << endl;
        c1.createNewShoppingCart(shoppingCart1Name);

        cout << "\033[1;32mOK\033[0m\n \n" << endl;

        cout <<
            "\nadding the following items from store 1: ... \n" <<
            "item #11 - Wooden Chair X 4\n" <<
            "item #12 - Bar Chair X 4\n" <<
            "item #21 - OfficeDesk X 1\n" << endl;

        c1.addItem(s1[11], shoppingCart1Name);
        c1.addItem(s1[11], shoppingCart1Name);
        c1.addItem(s1[11], shoppingCart1Name);
        c1.addItem(s1[11], shoppingCart1Name);

        c1.addItem(s1[12], shoppingCart1Name);
        c1.addItem(s1[12], shoppingCart1Name);
        c1.addItem(s1[12], shoppingCart1Name);
        c1.addItem(s1[12], shoppingCart1Name);

        c1.addItem(s1[21], shoppingCart1Name);

        std::set<Item>* shoppingCart1 = c1.getShoppingCart(shoppingCart1Name);

        const std::string shoppingCart2Name = "Groceries";

        cout <<
            "\nCreating a new shopping cart for the customer - c1.createNewShoppingCart(\"Groceries\"): ... \n" << endl;
        c1.createNewShoppingCart(shoppingCart2Name);

        cout <<
            "\nadding the following items from store 2: ... \n" <<
            "item #02 - Milk 3% 1 liter X 2\n" <<
            "item #21 - Mineral Water 6 pack X 1\n" <<
            "item #24 - Olive Oil 750ml X 1\n" <<
            "item #11 - Tomato 0.5 kg X 3\n" <<
            "item #12 - Carrot 0.5 kg X 1\n" <<
            "item #15 - Red Onion 0.5 kg X 1\n" <<
            "item #06 - Butter 100g X 2\n" <<
            "item #09 - Eggs size Large X 2\n" <<
            "item #20 - Pasta 1kg X 4\n" << endl;

        c1.addItem(s2[2], shoppingCart2Name);
        c1.addItem(s2[2], shoppingCart2Name);

        c1.addItem(s2[21], shoppingCart2Name);

        c1.addItem(s2[24], shoppingCart2Name);

        c1.addItem(s2[11], shoppingCart2Name);
        c1.addItem(s2[11], shoppingCart2Name);
        c1.addItem(s2[11], shoppingCart2Name);

        c1.addItem(s2[12], shoppingCart2Name);

        c1.addItem(s2[15], shoppingCart2Name);

        c1.addItem(s2[6], shoppingCart2Name);
        c1.addItem(s2[6], shoppingCart2Name);

        c1.addItem(s2[9], shoppingCart2Name);
        c1.addItem(s2[9], shoppingCart2Name);

        c1.addItem(s2[20], shoppingCart2Name);
        c1.addItem(s2[20], shoppingCart2Name);
        c1.addItem(s2[20], shoppingCart2Name);
        c1.addItem(s2[20], shoppingCart2Name);

        cout <<
            "\Checking shopping cart \"" << shoppingCart2Name << "\" ... \n" << endl;
        std::set<Item>* shoppingCart2 = c1.getShoppingCart(shoppingCart2Name);

        cout <<
            "\nremoving the following items from shopping basket \"Groceries\": ... \n" <<
            "item #11 - Tomato 0.5 kg X 3\n" <<
            "item #12 - Red Onion 0.5 kg X 1\n" <<
            "item #06 - Butter 100g X 2\n" <<
            "item #09 - Eggs size Large X 1\n" <<
            "item #20 - Pasta 1kg X 1\n" << endl;

        c1.removeItem(s2[11], shoppingCart2Name);
        c1.removeItem(s2[11], shoppingCart2Name);
        c1.removeItem(s2[11], shoppingCart2Name);

        c1.removeItem(s2[15], shoppingCart2Name);

        c1.removeItem(s2[6], shoppingCart2Name);
        c1.removeItem(s2[6], shoppingCart2Name);

        c1.removeItem(s2[9], shoppingCart2Name);

        c1.removeItem(s2[20], shoppingCart2Name);

    }
    catch (...)
    {
        std::cerr << "\033[1;31mTest crashed\033[0m\n \n" << endl;
        std::cerr << "\033[1;31mFAILED: The program crashed, check the following things:\n\033[0m\n \n" <<
            "1. Did you delete a pointer twice?\n2. Did you access index out of bounds?\n" <<
            "3. Did you remember to initialize array before accessing it?";
        return 2;
    }

    return 0;
}

int main()
{
    std::cout <<
        "###########################\n" <<
        "Exercise 10 - Magshistore\n" <<
        "Memory Test\n" <<
        "###########################\n" << std::endl;

    int testResult = test4Memory();

    cout << (testResult == 0 ? "\033[1;32m \n****** Ex10 Memory Tests Passed ******\033[0m\n \n" : "\033[1;31mEx10 Memory Tests Failed\033[0m\n \n") << endl;

    return testResult;
}