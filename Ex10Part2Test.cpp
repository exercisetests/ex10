#include <iostream>
#include <fstream>
#include <sstream>

#include "Store.h"

using std::cout;
using std::endl;

std::string getItemString(const Item& item)
{
	return
		"[Serial: " + item.getSerial() +
       ", Name: " + item.getName() +
		", Category: " + getItemCategoryString(item.getCategory()) +
		", Price: " + std::to_string(item.getPrice()) +
		", Amount: " + std::to_string(item.getCount()) + "]";
}

std::string readFileToString(const std::string fileName)
{
	std::ifstream inFile;
	inFile.open(fileName); //open the input file

	std::stringstream strStream;
	strStream << inFile.rdbuf(); //read the file
	std::string str = strStream.str(); //str holds the content of the file

	return str;
}

int test2Store()
{
	try
	{
		// Tests Ex10 part 2 - Store

		cout <<
			"*******************\n" <<
			"Test 2 - Store				\n" <<
			"*******************\n" << endl;

        ///////////////////////////
        // Checking Sort Methods //
        ///////////////////////////
	   cout <<
		    "Initializing Store1(\"MagshIKEA\", \"InventoryIKEA.csv\"): ... \n" << endl;
       Store s1("IKEA", "InventoryIKEA.csv");

       cout <<
           "\nSorting store items by Name: ... \n" << endl;

       std::string expected = readFileToString("output1a.txt");
       std::string got = s1.getSortedProductList(NAME);
       if (got == expected)
       {
           cout << "\033[1;32mTest Passed\033[0m\n \n" << endl;
       }
       else
       {
           cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
           cout << "Expected: \n" << expected << endl;
           std::cout << " \n" << endl;
           cout << "Got\n: " << got << endl;
           std::cout << " \n" << endl;
           system("./printMessage 10201");
           std::cout << " \n" << endl;
           return 10201;
       }

       cout <<
           "\nSorting store items by Price: ... \n" << endl;

       expected = readFileToString("output1b.txt");
       got = s1.getSortedProductList(PRICE);
       if (got == expected)
       {
           cout << "\033[1;32mTest Passed\033[0m\n \n" << endl;
       }
       else
       {
           cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
           cout << "Expected: \n" << expected << endl;
           std::cout << " \n" << endl;
           cout << "Got\n: " << got << endl;
           std::cout << " \n" << endl;
           system("./printMessage 10201");
           std::cout << " \n" << endl;
           return 10201;
       }

       cout <<
           "\nSorting store items by Serial Number: ... \n" << endl;

       expected = readFileToString("output1c.txt");
       got = s1.getSortedProductList(SERIAL);
       if (got == expected)
       {
           cout << "\033[1;32mTest Passed\033[0m\n \n" << endl;
       }
       else
       {
           cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
           cout << "Expected: \n" << expected << endl;
           std::cout << " \n" << endl;
           cout << "Got\n: " << got << endl;
           std::cout << " \n" << endl;
           system("./printMessage 10201");
           std::cout << " \n" << endl;
           return 10201;
       }

       cout <<
           "Initializing Store2(\"Shefa Isaschar\", \"InventorySuperMarket.csv\"): ... \n" << endl;
       Store s2("Shefa Isaschar", "InventorySuperMarket.csv");

       cout <<
           "\nSorting store items by Name: ... \n" << endl;

       expected = readFileToString("output2a.txt");
       got = s2.getSortedProductList(NAME);
       if (got == expected)
       {
           cout << "\033[1;32mTest Passed\033[0m\n \n" << endl;
       }
       else
       {
           cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
           cout << "Expected: \n" << expected << endl;
           std::cout << " \n" << endl;
           cout << "Got\n: " << got << endl;
           std::cout << " \n" << endl;
           system("./printMessage 10201");
           std::cout << " \n" << endl;
           return 10201;
       }

       //    cout <<
       //        "\nSorting store items by Category: ... \n" << endl;

       //    expected = readFileToString("output2b.txt");
       //    got = s2.getSortedProductList(CATEGORY);
       //    cout << "Expected:\n" << expected << endl;
       //    cout << "Got\n:\n" << got << std::endl;
       //    if (got != expected)
       //    {
       //        std::cout << "FAILED: Store information is not as expected\n" <<
       //            "check Store Constructor and functions Store::getSortedProductList(SortingCriteria) \n";
       //        return false;
       //    }

       cout <<
           "Initializing Store3(\"MagshiPharm\", \"InventoryPharm.csv\"): ... \n" << endl;
       Store s3("MagshiPharm", "InventoryPharm.csv");

       cout <<
           "\nSorting store items by Name: ... \n" << endl;

       expected = readFileToString("output3a.txt");
       got = s3.getSortedProductList(NAME);
       if (got == expected)
       {
           cout << "\033[1;32mTest Passed\033[0m\n \n" << endl;
       }
       else
       {
           cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
           cout << "Expected: \n" << expected << endl;
           std::cout << " \n" << endl;
           cout << "Got\n: " << got << endl;
           std::cout << " \n" << endl;
           system("./printMessage 10201");
           std::cout << " \n" << endl;
           return 10201;
       }

       //    cout <<
       //        "\nSorting store items by Category: ... \n" << endl;

       //    expected = readFileToString("output3b.txt");
       //    got = s3.getSortedProductList(CATEGORY);
       //    cout << "Expected:\n" << expected << endl;
       //    cout << "Got\n:\n" << got << std::endl;
       //    if (got != expected)
       //    {
       //        std::cout << "FAILED: Store information is not as expected\n" <<
       //            "check Store Constructor and functions Store::getSortedProductList(SortingCriteria) \n";
       //        return false;
       //    }

          //////////////////////////////
          // Checking Filter Methods  //
          //////////////////////////////

       cout <<
           "\nFiltering store items by Category = FOOD: ... \n" << endl;

       expected = readFileToString("output3c.txt");
       got = s3.getProductListFilteredByCategory(FOOD);
       if (got == expected)
       {
           cout << "\033[1;32mTest Passed\033[0m\n \n" << endl;
       }
       else
       {
           cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
           cout << "Expected: \n" << expected << endl;
           std::cout << " \n" << endl;
           cout << "Got\n: " << got << endl;
           std::cout << " \n" << endl;
           system("./printMessage 10202");
           std::cout << " \n" << endl;
           return 10202;
       }

       cout <<
           "\nFiltering store items by Category = CLEANING: ... \n" << endl;

       expected = readFileToString("output3d.txt");
       got = s3.getProductListFilteredByCategory(CLEANING);
       if (got == expected)
       {
           cout << "\033[1;32mTest Passed\033[0m\n \n" << endl;
       }
       else
       {
           cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
           cout << "Expected: \n" << expected << endl;
           std::cout << " \n" << endl;
           cout << "Got\n: " << got << endl;
           std::cout << " \n" << endl;
           system("./printMessage 10202");
           std::cout << " \n" << endl;
           return 10202;
       }

       cout <<
           "\nFiltering store items by Category = PHARM: ... \n" << endl;

       expected = readFileToString("output3e.txt");
       got = s3.getProductListFilteredByCategory(PHARM);
       if (got == expected)
       {
           cout << "\033[1;32mTest Passed\033[0m\n \n" << endl;
       }
       else
       {
           cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
           cout << "Expected: \n" << expected << endl;
           std::cout << " \n" << endl;
           cout << "Got\n: " << got << endl;
           std::cout << " \n" << endl;
           system("./printMessage 10202");
           std::cout << " \n" << endl;
           return 10202;
       }
	}
    catch (...)
    {
        std::cerr << "\033[1;31mTest crashed\033[0m\n \n" << endl;
        std::cerr << "\033[1;31mFAILED: The program crashed, check the following things:\n\033[0m\n \n" <<
            "1. Did you delete a pointer twice?\n2. Did you access index out of bounds?\n" <<
            "3. Did you remember to initialize array before accessing it?";
        return 2;
    }

    return 0;
}

int main()
{
	std::cout <<
		"###########################\n" <<
		"Exercise 10 - Magshistore\n" <<
		"Part 2 - Store\n" << 
		"###########################\n" << std::endl;

	int testResult = test2Store();

    cout << (testResult == 0 ? "\033[1;32m \n****** Ex10 Part 2 Tests Passed ******\033[0m\n \n" : "\033[1;31mEx10 Part 2 Tests Failed\033[0m\n \n") << endl;

    return testResult;
}